import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BeforeLoginComponent } from './layout/before-login/before-login.component';
import { AfterLoginComponent } from './layout/after-login/after-login.component';
import { HttpClientModule } from '@angular/common/http';
import {GlobalServiceService} from './shared/global-service.service'
@NgModule({
  declarations: [
    AppComponent,
    BeforeLoginComponent,
    AfterLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,RouterModule,BrowserAnimationsModule,HttpClientModule,ToastrModule.forRoot({ progressBar: true, timeOut: 4000 }),
  ],
  providers: [GlobalServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
