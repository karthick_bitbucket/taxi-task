import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BeforeLoginComponent } from './layout/before-login/before-login.component';
import { AfterLoginComponent } from './layout/after-login/after-login.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: '',
    component: BeforeLoginComponent,
    children: [
      {
        path: 'login',
        loadChildren: () => import('./before-login/login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'register',
        loadChildren: () => import('./before-login/register/register.module').then(m => m.RegisterModule)
      }
    ]
  }
  ,
  {
    path: '',
    component: AfterLoginComponent,
    children: [
      {
        path: 'profile',
        loadChildren: () => import('./after-login/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'booking-user',
        loadChildren: () => import('./after-login/booking-user/booking-user.module').then(m => m.BookingUserModule)
      },
      {
        path: 'booking-driver',
        loadChildren: () => import('./after-login/booking-driver/booking-driver.module').then(m => m.BookingDriverModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
