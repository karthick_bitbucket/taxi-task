import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.scss']
})
export class AfterLoginComponent implements OnInit {

  constructor() {
    this.userRole = localStorage.getItem('userRole');
  }
  userRole;
  active='profile';
  ngOnInit(): void {
  }

}
