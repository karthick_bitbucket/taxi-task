import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import { RouterModule} from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,RouterModule,FormsModule,ReactiveFormsModule
  ]
})
export class RegisterModule { }
