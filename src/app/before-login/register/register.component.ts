import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private service: GlobalServiceService,
    private FormBuilder: FormBuilder,
    private Router: Router,
    private activatedRoute: ActivatedRoute,
    private ToastrService: ToastrService) {

  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.userRole = params.role;
    })
    this.formInit();
  }
  // Variables
  // ============================
  registerForm: FormGroup;
  url = 'userTable';
  userRole;
  // ===================================================================================================================



  // Functionalities
  // ============================
  formInit() {
    this.registerForm = this.FormBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      role: new FormControl(this.userRole),
    })
  }

  register() {
    this.setValidators(this.registerForm);
    if (this.registerForm.valid) {
      this.service.post_data(this.url, this.registerForm.value).subscribe((result) => {
        console.log(result);
        this.registerForm.reset();
        this.ToastrService.success('Registered Successfully');
        this.Router.navigate(['/login']);
      }, (error) => {
        console.log(error);
        this.ToastrService.error(error)
      })
    }
    else {
      this.validateAllFormFields(this.registerForm);
    }
  }

  // ===================================================================================================================



  // VALIDATION
  // ============================
  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Email is not valid.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone Number is required.' }
    ],
    'userName': [
      { type: 'required', message: 'Username is required.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' }
    ]
  };
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsPending({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      if (String(field) == 'email') {
        formGroup.get(field).setValidators([Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]);
      } else {
        formGroup.get(field).setValidators(Validators.required);
      }
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ===================================================================================================================

}
