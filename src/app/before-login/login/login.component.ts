import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private service: GlobalServiceService,
    private FormBuilder: FormBuilder,
    private Router: Router,
    private ToastrService: ToastrService) { }

  ngOnInit(): void {
    this.getUserList();
    this.formInit();
  }
  // Variables
  // ============================
  loginForm: FormGroup;
  url = 'userTable';
  userList: any = [];
  // ===================================================================================================================



  // Functionalities
  // ============================
  formInit() {
    this.loginForm = this.FormBuilder.group({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    })
  }
  getUserList() {
    this.service.get_lists(this.url).subscribe((res) => {
      this.userList = [];
      this.userList=res;
    }, (error) => {
      console.log(error)
      this.ToastrService.error(error)
    })
  }
  login() {
    this.setValidators(this.loginForm);
    if (this.loginForm.valid) {
      if (this.userList.length > 0) {
        let user = this.userList.find(u => u.userName == this.loginForm.get('userName').value && u.password == this.loginForm.get('password').value);
        if (user) {
          this.ToastrService.success("Welcome " + user.name)
          localStorage.setItem('userRole', user.role)
          localStorage.setItem('userId', user.id)
          this.Router.navigate(['/profile']);
        } else {
          this.ToastrService.error('User Invalid');
        }
      } else {
        this.ToastrService.error('User Invalid');
      }
    }
    else {
      this.validateAllFormFields(this.loginForm);
    }
  }

  // ===================================================================================================================



  // VALIDATION
  // ============================
  validation_messages = {
    'userName': [
      { type: 'required', message: 'Username is required.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' }
    ]
  };
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsPending({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      formGroup.get(field).setValidators(Validators.required);
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ===================================================================================================================

}
