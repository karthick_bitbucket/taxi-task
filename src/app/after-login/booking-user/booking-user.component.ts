import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-booking-user',
  templateUrl: './booking-user.component.html',
  styleUrls: ['./booking-user.component.scss']
})
export class BookingUserComponent implements OnInit {

  constructor(private service: GlobalServiceService,
    private FormBuilder: FormBuilder,
    private Router: Router,
    private ToastrService: ToastrService) {
    this.userId = localStorage.getItem('userId');
  }

  ngOnInit(): void {
    this.getUserList();
    // this.getDriverConformList();
    // this.getUserBookingDetails();
    this.formInit();
  }
  // Variables
  // ============================
  bookingForm: FormGroup;
  url = 'userBooking';
  DriverUrl = "driverBooking";
  userUrl = "userTable";
  userId;
  bookingList: any = [];
  driverConfirmList: any = [];
  userList: any = [];
  // ===================================================================================================================



  // Functionalities
  // ============================
  formInit() {
    this.bookingForm = this.FormBuilder.group({
      userId: new FormControl(this.userId),
      originFrom: new FormControl('', Validators.required),
      destinationTo: new FormControl('', Validators.required),
      pickupTime: new FormControl('', Validators.required),
    })
  }
  getUserList() {
    this.service.get_lists(this.userUrl).subscribe((res) => {
      this.userList = res;
      this.getDriverConformList();
    })
  }
  getDriverConformList() {
    this.service.get_lists(this.DriverUrl).subscribe((res) => {
      let arr = res;
      arr.forEach(element => {
        element.user = this.userList.filter(r => r.id == element.userId)[0].name;
      });
      this.driverConfirmList = arr;
      this.getUserBookingDetails();
    })
  }

  getUserBookingDetails() {
    this.service.get_lists(this.url).subscribe((res) => {
      let arr = [];
      arr = res;
      if (arr.length > 0) {
        this.bookingList = arr.filter(e => e.userId == this.userId);
        this.bookingList.forEach((element, i) => {
          element.driverConfirmation = this.driverConfirmList.filter(l => l.bookingId == element.id)[0];
        });
      }
      console.log(this.bookingList)
    }, (error) => {
      this.ToastrService.error(error);
    })
  }
  book() {
    this.setValidators(this.bookingForm);
    if (this.bookingForm.valid) {
      this.service.post_data(this.url, this.bookingForm.value).subscribe((res) => {
        console.log(res);
        this.ToastrService.success('Taxi Booked Successfully')
        this.getUserBookingDetails();
        this.reset();
      })
    }
    else {
      this.validateAllFormFields(this.bookingForm);
    }
  }
  reset() {
    this.bookingForm.reset();
    this.bookingForm.get('userId').patchValue(this.userId);
  }
  // ===================================================================================================================



  // VALIDATION
  // ============================
  validation_messages = {
    'originFrom': [
      { type: 'required', message: 'Origin From is required.' }
    ],
    'destinationTo': [
      { type: 'required', message: 'Destination To is required.' }
    ],
    'pickupTime': [
      { type: 'required', message: 'Pickup Time is required.' }
    ]
  };
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsPending({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      formGroup.get(field).setValidators(Validators.required);
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ===================================================================================================================

}
