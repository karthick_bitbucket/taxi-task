import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingUserRoutingModule } from './booking-user-routing.module';
import { BookingUserComponent } from './booking-user.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [BookingUserComponent],
  imports: [
    CommonModule,
    BookingUserRoutingModule, FormsModule, ReactiveFormsModule
  ]
})
export class BookingUserModule { }
