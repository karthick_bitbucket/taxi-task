import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingUserComponent } from './booking-user.component';
const routes: Routes = [{
  path:'',
  component:BookingUserComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingUserRoutingModule { }
