import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private service: GlobalServiceService, private toastrService: ToastrService) {
    this.userId = localStorage.getItem('userId');
  }

  ngOnInit(): void {
    this.getUserInfo();
  }
  userId;
  userInfo:any={};
  url = "userTable"
  getUserInfo() {
    this.service.get_data(this.url, this.userId).subscribe((res) => {
      this.userInfo=res;
      console.log(res)
    }, (error) => {
      this.toastrService.error(error)
    })
  }
}
