import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingDriverRoutingModule } from './booking-driver-routing.module';
import { BookingDriverComponent } from './booking-driver.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [BookingDriverComponent],
  imports: [
    CommonModule,
    BookingDriverRoutingModule,FormsModule, ReactiveFormsModule
  ]
})
export class BookingDriverModule { }
