import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-booking-driver',
  templateUrl: './booking-driver.component.html',
  styleUrls: ['./booking-driver.component.scss']
})
export class BookingDriverComponent implements OnInit {

  constructor(private service: GlobalServiceService,
    private FormBuilder: FormBuilder,
    private Router: Router,
    private ToastrService: ToastrService) {
    this.userId = localStorage.getItem('userId');
  }
  confirmationForm: FormGroup;
  userUrl = 'userBooking';
  url = 'driverBooking';
  userBookingList = [];
  userBookingListSearched = [];
  currentLocation = '';
  userId;
  rideInfo = {};
  ngOnInit(): void {
    this.getBookingList();
    this.formInit();
  }
  formInit() {
    this.confirmationForm = this.FormBuilder.group({
      userId: new FormControl(this.userId),
      bookingId: new FormControl(''),
      price: new FormControl('', Validators.required),
      pickupTime: new FormControl('', Validators.required),
    })
  }

  getBookingList() {
    this.service.get_lists(this.userUrl).subscribe((res) => {
      this.userBookingList = res;
      this.userBookingListSearched = res;
    })
  }
  search() {
    if (this.currentLocation) {
      this.userBookingListSearched = this.userBookingList.filter(s => s.originFrom == this.currentLocation);
    }
  }

  SelectRide(id) {
    this.confirmationForm.get('bookingId').patchValue(id);
    this.rideInfo = this.userBookingList.filter(s => s.id == id)[0];
  }


  confirm() {
    this.setValidators(this.confirmationForm);
    if (this.confirmationForm.valid) {
      this.service.post_data(this.url, this.confirmationForm.value).subscribe((res) => {
        console.log(res)
        this.ToastrService.success('Taxi Booked Successfully')
        this.reset();
      })
    }
    else {
      this.validateAllFormFields(this.confirmationForm);
    }
  }
  reset() {
    this.confirmationForm.reset();
    this.rideInfo={};
    this.confirmationForm.get('userId').patchValue(this.userId);
  }

  // VALIDATION
  // ============================
  validation_messages = {
    'price': [
      { type: 'required', message: 'Price is required.' }
    ],
    'pickupTime': [
      { type: 'required', message: 'Pickup Time is required.' }
    ]
  };
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsPending({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      formGroup.get(field).setValidators(Validators.required);
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ===================================================================================================================



}
