import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalServiceService {

  constructor(private http: HttpClient) { 

  }

  post_data(particle_url, obj): Observable<any> {
    return this.http.post(environment.API_URL + particle_url, obj);
  }

  get_lists(particle_url): Observable<any> {
    return this.http.get(environment.API_URL + particle_url);
  }

  get_data(particle_url, id): Observable<any> {
    return this.http.get(environment.API_URL + particle_url + '/' + id);
  }

  put_data(particle_url, id, obj): Observable<any> {
    const api_url = environment.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.put(url, JSON.stringify(obj));
  }

  delete_data(particle_url, id): Observable<any> {
    const api_url = environment.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.delete(url);
  }
}
